CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module is intended to solve a very simple problem: if you have "secured" pages to which you want to link, but a user is not logged in, the menu will hide it. If you point a user to user/login?destination=the-page, the link will disappear for logged in users.

Point your users to checklink?destination=the-page to allow the link to show, but redirect users appropriately.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/checklink

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/checklink

REQUIREMENTS
------------

None

RECOMMENDED MODULES
-------------------

 None

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Visit the configuration page under Admin > Configuration > Web Services

TROUBLESHOOTING
---------------


FAQ
---

Q: I don't have any questions.

A: Great!

MAINTAINERS
-----------

Current maintainers:
 * Adam Weiss (greatmatter) - https://www.drupal.org/u/greatmatter

This project has been sponsored by:
 * Great Matter
   We create the software you need most.
   Specializing in Drupal-based systems, Great Matter designs, develops,
   and supports amazing software solutions for the most complicated needs.
   Visit https://www.greatmatter.com for more information.
